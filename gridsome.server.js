// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const axios = require('axios')
const HttpProxy = require("./httpProxy.js")

const LIMIT_FRAMALIBRE = 1272;

module.exports = function (api) {
  api.loadSource(async actions => {

    const _proxy = new HttpProxy();
    
    const annuaireUrl = 'https://framalibre.org/category/annuaires/json';
    const annuaireData = await (await _proxy.fetchJson(annuaireUrl));

    const categorieCollection = actions.addCollection('CategorieFramalibre')

    const elementCollection = actions.addCollection('ElementFramalibre')

    const tagCollection = actions.addCollection('TagFramalibre')
    elementCollection.addReference('tags', 'TagFramalibre')

    for (const [indexCategory, item] of annuaireData.nodes.entries()) {
      // console.log(item)
      console.log(`Processing category ${indexCategory + 1} / ${annuaireData.nodes.length}`)
      const categoryId = item.node['Identifiant (ID) du terme'].replace(',', '')
      console.log(`Adding category ${categoryId} (${item.node.Nom})`)
      const category = categorieCollection.addNode({
        internalId: categoryId,
        id: item.node.Nom
      })

      const categoryURL = `https://framalibre.org/taxonomy/term/${categoryId}/json`
      const categoryData = await (await _proxy.fetchJson(categoryURL));

      for (const [indexNode, categoryItem] of categoryData.nodes.entries()) {
        console.log(`Processing node ${indexNode + 1} / ${categoryData.nodes.length}`)
        const itemURL = `https://framalibre.org${categoryItem.node.json_id}`;
        const itemData = await (await _proxy.fetchJson(itemURL));

        const itemElement = itemData.nodes[0].node;

        console.log(`Adding element ${itemElement.Nid} (${itemElement.Titre})`)
        const etiquettes = itemElement.Étiquettes || ''
        const tags = etiquettes.split(',').map(item => item.trim())
        tags.forEach(tag => {
          tagCollection.addNode({ id: tag })
        });
        const val = {
          id: itemElement.Chemin.slice(9),
          category: actions.store.createReference(category),
          tags: tags,
          internalId: itemElement.id,
          resume: itemElement.body_1,
          logo: itemElement.Logo.src,
          ...itemElement
        };
        delete val['Capture d&#039;écran']
        delete val['Auteur de l&#039;illustration']
        delete val['Logo']
        elementCollection.addNode(val)
      }
    }

    const wikidataQuery = `SELECT DISTINCT ?item ?logo ?pic ?logiciel_libreLabel ?logiciel_libreDescription ?identifiant_Framalibre WHERE {
      BIND(REPLACE(STR(?logiciel_libre), "^.*/", "") AS ?item)
      SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
      ?logiciel_libre wdt:P31/wdt:P279* wd:Q341 .
      ?logiciel_libre wdt:P18 ?pic .
      ?logiciel_libre wdt:P154 ?logo
      OPTIONAL { ?logiciel_libre wdt:P4107 ?identifiant_Framalibre. }
    }
    ORDER BY ?logiciel_libreLabel`

    const wikidataURL = `https://query.wikidata.org/sparql?query=${encodeURIComponent(wikidataQuery)}`

    const wikiDataCollection = actions.addCollection('WikidataSoftware')
    wikiDataCollection.addReference('framalibre', 'ElementFramalibre')

    const { results: { bindings: wikidata }} = await (await _proxy.fetchJson(wikidataURL));

    wikidata.forEach(item => {
      Object.keys(item).forEach(property => {
        item[property] = item[property].value;
      });
      item.framalibre = item.identifiant_Framalibre
      wikiDataCollection.addNode(item);
    })
    
  })

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  })
}
