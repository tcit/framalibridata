// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Framalibre & Wikidata',
  outDir: 'public',
  pathPrefix: process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
  plugins: [
    // {
    //   use: "gridsome-source-wikidata",
    //   options: {
    //     url: "https://query.wikidata.org/sparql",
    //     sparql: `SELECT DISTINCT ?item ?logiciel_libreLabel ?logiciel_libreDescription ?identifiant_Framalibre WHERE {
    //       BIND(REPLACE(STR(?logiciel_libre), "^.*/", "") AS ?item)
    //       SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    //       ?logiciel_libre wdt:P31 wd:Q341.
    //       OPTIONAL { ?logiciel_libre wdt:P4107 ?identifiant_Framalibre. }
    //     }
    //     ORDER BY ?logiciel_libreLabel
    //     LIMIT 1000`,
    //     typeName: "WikidataSoftware",
    //     verbose: "true"
    //   }
    // },
    {
      use: 'gridsome-plugin-flexsearch',
      options: {
        collections: [
          {
            typeName: 'WikidataSoftware',
            indexName: 'WikidataSoftware',
            fields: ['logiciel_libreLabel', 'logiciel_libreDescription', 'logo']
          },
          {
            typeName: 'CategorieFramalibre',
            indexName: 'categorieFramalibre',
            fields: ['id']
          },
          {
            typeName: 'ElementFramalibre',
            indexName: 'ElementFramalibre',
            fields: ['id', 'Titre', 'Texte_descriptif', 'logo']
          }
        ],
        searchFields: ['id', 'logiciel_libreLabel', 'Titre']
      }
    }
  ],
  templates: {
    WikidataSoftware: '/wikidata/:item',
    CategorieFramalibre: '/category/:id',
    TagFramalibre: '/tag/:id',
    ElementFramalibre: '/:id'
  }
}
